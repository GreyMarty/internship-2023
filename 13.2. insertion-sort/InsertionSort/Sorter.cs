﻿using System;

// ReSharper disable InconsistentNaming
namespace InsertionSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with insertion sort algorithm.
        /// </summary>
        public static void InsertionSort(this int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 1; i < array.Length; i++)
            {
                for (int j = i; j > 0 && array[j] < array[j - 1]; j--)
                {
                    Swap(ref array[j], ref array[j - 1]);
                }
            }
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive insertion sort algorithm.
        /// </summary>
        public static void RecursiveInsertionSort(this int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            RecursiveInsertionSort(array, 1);
        }

        public static void RecursiveInsertionSort(this int[] array, int i)
        {
            if (i >= array.Length)
            {
                return;
            }

            for (int j = i; j > 0 && array[j] < array[j - 1]; j--)
            {
                Swap(ref array[j], ref array[j - 1]);
            }

            RecursiveInsertionSort(array, i + 1);
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            var temp = a;
            a = b;
            b = temp;
        }
    }
}
