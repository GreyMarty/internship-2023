﻿using System;

namespace Numbers
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Obtains formalized information in the form of an enum <see cref="ComparisonSigns"/>
        /// about the relationship of the order of two adjacent digits for all digits of a given number.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>Information in the form of an enum <see cref="ComparisonSigns"/>
        /// about the relationship of the order of two adjacent digits for all digits of a given number
        /// or null if the information is not defined.</returns>
        public static ComparisonSigns? GetTypeComparisonSigns(this long number)
        {
            if (number == long.MinValue)
            {
                return ComparisonSigns.Equals | ComparisonSigns.MoreThan | ComparisonSigns.LessThan;
            }

            number = Math.Abs(number);

            if (number < 10)
            {
                return null;
            }

            var flags = (ComparisonSigns)0;

            var prevDigit = number % 10;
            number /= 10;

            while (number > 0)
            {
                var digit = number % 10;

                if (digit > prevDigit)
                {
                    flags |= ComparisonSigns.MoreThan;
                }
                else if (digit < prevDigit)
                {
                    flags |= ComparisonSigns.LessThan;
                }
                else
                {
                    flags |= ComparisonSigns.Equals;
                }

                prevDigit = digit;
                number /= 10;
            }

            return flags;
        }

        /// <summary>
        /// Gets information in the form of a string about the type of sequence that the digit of a given number represents.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>The information in the form of a string about the type of sequence that the digit of a given number represents.</returns>
        public static string GetTypeOfDigitsSequence(this long number)
        {
            var signs = GetTypeComparisonSigns(number);

            if (signs is null)
            {
                return "One digit number.";
            }

            switch (signs)
            {
                case ComparisonSigns.Equals:
                    return "Monotonous.";

                case ComparisonSigns.MoreThan:
                    return "Strictly Decreasing.";

                case ComparisonSigns.LessThan:
                    return "Strictly Increasing.";

                case ComparisonSigns.MoreThan | ComparisonSigns.Equals:
                    return "Decreasing.";

                case ComparisonSigns.LessThan | ComparisonSigns.Equals:
                    return "Increasing.";

                default:
                    return "Unordered.";
            }
        }
    }
}
