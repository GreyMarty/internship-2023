﻿namespace LouVuiDateCode
{
    public static class DateCodeParser
    {
        /// <summary>
        /// Parses a date code and returns a <see cref="manufacturingYear"/> and <see cref="manufacturingMonth"/>.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseEarly1980Code(string dateCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length < 3)
            {
                throw new ArgumentException($"{nameof(dateCode)} is not valid LouVui date code.", nameof(dateCode));
            }

            string yearPart = dateCode.Substring(0, 2);
            string monthPart = dateCode.Substring(2);

            if (!uint.TryParse(yearPart, out var year) || year >= 90 || year < 80)
            {
                throw new ArgumentException("Year part must be in range [1980, 1990).", nameof(dateCode));
            }

            if (!uint.TryParse(monthPart, out var month) || month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be in range [1, 12].", nameof(dateCode));
            }

            manufacturingYear = 1900 + year;
            manufacturingMonth = month;
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseLate1980Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            var yearly1980Code = dateCode[..^2];
            ParseEarly1980Code(yearly1980Code, out manufacturingYear, out manufacturingMonth);

            var locationCode = dateCode[^2..];
            var locationCountries = CountryParser.GetCountry(locationCode);

            if (locationCountries.Length == 0)
            {
                throw new ArgumentException($"Factory location code is not valid.", nameof(dateCode));
            }

            factoryLocationCountry = locationCountries;
            factoryLocationCode = locationCode;
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A six number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void Parse1990Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException($"{nameof(dateCode)} is not valid LouVui date code.", nameof(dateCode));
            }

            var locationCode = dateCode[..2];

            var monthPart = string.Join(string.Empty, dateCode[2], dateCode[4]);
            var yearPart = string.Join(string.Empty, dateCode[3], dateCode[5]);

            if (!uint.TryParse(yearPart, out var year) || (year > 6 && year < 90))
            {
                throw new ArgumentException("Year part must be in range [1990, 2006).", nameof(dateCode));
            }

            if (!uint.TryParse(monthPart, out var month) || month < 1 || month > 12)
            {
                throw new ArgumentException("Month must be in range [1, 12].", nameof(dateCode));
            }

            var locationCountries = CountryParser.GetCountry(locationCode);

            if (locationCountries.Length == 0)
            {
                throw new ArgumentException($"Factory location code is not valid.", nameof(dateCode));
            }

            factoryLocationCountry = locationCountries;
            factoryLocationCode = locationCode;
            manufacturingYear = year + (year > 6 ? 1900u : 2000u);
            manufacturingMonth = month;
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingWeek"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A six number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingWeek">A manufacturing week to return.</param>
        public static void Parse2007Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException($"{nameof(dateCode)} is not valid LouVui date code.", nameof(dateCode));
            }

            var locationCode = dateCode[..2];

            var weekPart = string.Join(string.Empty, dateCode[2], dateCode[4]);
            var yearPart = string.Join(string.Empty, dateCode[3], dateCode[5]);

            if (!uint.TryParse(yearPart, out var year) || year < 7)
            {
                throw new ArgumentException("Year part must be greater or equal to 2007.", nameof(dateCode));
            }

            var maxWeeks = year == 20 ? 53 : 52;

            if (!uint.TryParse(weekPart, out var week) || week < 1 || week > maxWeeks)
            {
                throw new ArgumentException("Week must be in range [1, 53).", nameof(dateCode));
            }

            var locationCountries = CountryParser.GetCountry(locationCode);

            if (locationCountries.Length == 0)
            {
                throw new ArgumentException($"Factory location code is not valid.", nameof(dateCode));
            }

            factoryLocationCountry = locationCountries;
            factoryLocationCode = locationCode;
            manufacturingYear = year + 2000;
            manufacturingWeek = week;
        }
    }
}
