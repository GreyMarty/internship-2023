﻿namespace LouVuiDateCode
{
    public static class CountryParser
    {
        /// <summary>
        /// Gets a an array of <see cref="Country"/> enumeration values for a specified factory location code. One location code can belong to many countries.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <returns>An array of <see cref="Country"/> enumeration values.</returns>
        public static Country[] GetCountry(string factoryLocationCode)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(factoryLocationCode);
            }

            var codes = new Dictionary<string, Country[]>
            {
                { "A0", new[] { Country.France } },
                { "A1", new[] { Country.France } },
                { "A2", new[] { Country.France } },
                { "AA", new[] { Country.France } },
                { "AH", new[] { Country.France } },
                { "AN", new[] { Country.France } },
                { "AR", new[] { Country.France } },
                { "AS", new[] { Country.France } },
                { "BA", new[] { Country.France } },
                { "BJ", new[] { Country.France } },
                { "BU", new[] { Country.France } },
                { "DR", new[] { Country.France } },
                { "DU", new[] { Country.France } },
                { "DT", new[] { Country.France } },
                { "CO", new[] { Country.France } },
                { "CT", new[] { Country.France } },
                { "CX", new[] { Country.France } },
                { "ET", new[] { Country.France } },
                { "FL", new[] { Country.France, Country.USA } },
                { "LW", new[] { Country.France, Country.Spain } },
                { "MB", new[] { Country.France } },
                { "MI", new[] { Country.France } },
                { "NO", new[] { Country.France } },
                { "RA", new[] { Country.France } },
                { "RI", new[] { Country.France } },
                { "SD", new[] { Country.France, Country.USA } },
                { "SF", new[] { Country.France } },
                { "SL", new[] { Country.France } },
                { "SN", new[] { Country.France } },
                { "SP", new[] { Country.France } },
                { "SR", new[] { Country.France } },
                { "TJ", new[] { Country.France } },
                { "TH", new[] { Country.France } },
                { "TR", new[] { Country.France } },
                { "TS", new[] { Country.France } },
                { "VI", new[] { Country.France } },
                { "VX", new[] { Country.France } },
                { "LP", new[] { Country.Germany } },
                { "OL", new[] { Country.Germany } },
                { "BC", new[] { Country.Italy } },
                { "BO", new[] { Country.Italy } },
                { "CE", new[] { Country.Italy } },
                { "FO", new[] { Country.Italy } },
                { "MA", new[] { Country.Italy } },
                { "OB", new[] { Country.Italy } },
                { "RC", new[] { Country.Italy } },
                { "RE", new[] { Country.Italy } },
                { "SA", new[] { Country.Italy } },
                { "TD", new[] { Country.Italy } },
                { "CA", new[] { Country.Spain } },
                { "LO", new[] { Country.Spain } },
                { "LB", new[] { Country.Spain } },
                { "LM", new[] { Country.Spain } },
                { "GI", new[] { Country.Spain } },
                { "DI", new[] { Country.Switzerland } },
                { "FA", new[] { Country.Switzerland } },
                { "FC", new[] { Country.USA } },
                { "FH", new[] { Country.USA } },
                { "LA", new[] { Country.USA } },
                { "OS", new[] { Country.USA } },
            };

            return codes.TryGetValue(factoryLocationCode, out var value) ? value : Array.Empty<Country>();
        }
    }
}
