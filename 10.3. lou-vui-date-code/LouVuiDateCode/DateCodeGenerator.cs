﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace LouVuiDateCode
{
    public static class DateCodeGenerator
    {
        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1980 || manufacturingYear >= 1990)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            return $"{manufacturingYear % 100}{manufacturingMonth}";
        }

        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(DateTime manufacturingDate)
        {
            return GenerateEarly1980Code((uint)manufacturingDate.Year, (uint)manufacturingDate.Month);
        }

        /// <summary>
        /// Generates a date code using rules from late 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (!Regex.IsMatch(factoryLocationCode, @"^[A-Za-z]{2}$"))
            {
                throw new ArgumentException("Specified factory location code is invalid.", nameof(factoryLocationCode));
            }

            var culture = new CultureInfo("en-US");

            return $"{GenerateEarly1980Code(manufacturingYear, manufacturingMonth)}{factoryLocationCode.ToUpper(culture)}";
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generates a date code using rules from late 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            return GenerateLate1980Code(factoryLocationCode, (uint)manufacturingDate.Year, (uint)manufacturingDate.Month);
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 1990 || manufacturingYear > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            if (!Regex.IsMatch(factoryLocationCode, @"^[A-Za-z]{2}$"))
            {
                throw new ArgumentException("Specified factory location code is invalid.", nameof(factoryLocationCode));
            }

            var culture = new CultureInfo("en-US");

            var yearString = (manufacturingYear % 100).ToString("00", culture);
            var monthString = manufacturingMonth.ToString("00", culture);

            return $"{factoryLocationCode.ToUpper(culture)}{monthString[0]}{yearString[0]}{monthString[1]}{yearString[1]}";
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            return Generate1990Code(factoryLocationCode, (uint)manufacturingDate.Year, (uint)manufacturingDate.Month);
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingWeek">A manufacturing week number.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingWeek == 0 || manufacturingWeek > 53 || (manufacturingYear >= 2017 && manufacturingYear < 2020 && manufacturingWeek >= 53))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingWeek));
            }

            if (!Regex.IsMatch(factoryLocationCode, @"^[A-Za-z]{2}$"))
            {
                throw new ArgumentException("Specified factory location code is invalid.", nameof(factoryLocationCode));
            }

            var culture = new CultureInfo("en-US");

            var yearString = (manufacturingYear % 100).ToString("00", culture);
            var weekString = manufacturingWeek.ToString("00", culture);

            return $"{factoryLocationCode.ToUpper(culture)}{weekString[0]}{yearString[0]}{weekString[1]}{yearString[1]}";
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            var week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(manufacturingDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var year = week >= 52 && manufacturingDate <= new DateTime(manufacturingDate.Year, 1, 3)
                ? manufacturingDate.Year - 1
                : manufacturingDate.Year;
            return Generate2007Code(factoryLocationCode, (uint)year, (uint)week);
        }
    }
}
