﻿using System;
using System.Collections;

// ReSharper disable InconsistentNaming
namespace QuickSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with quick sort algorithm using <paramref name="byKey">.
        /// <param name="array">Array to sort.</param>
        /// <param name="byKey">Sort key selector</param>
        /// </summary>
        public static void QuickSort<T>(this T[]? array, Comparison<T>? compare = null)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            compare ??= Comparer<T>.Default.Compare;

            QuickSort(array, 0, array.Length - 1, compare);
        }

        private static void QuickSort<T>(T[] array, int start, int end, Comparison<T> compare)
        {
            if (start >= end)
            {
                return;
            }

            var pivot = Partition(array, start, end, compare);
            QuickSort(array, start, pivot, compare);
            QuickSort(array, pivot + 1, end, compare);
        }

        private static int Partition<T>(T[] array, int start, int end, Comparison<T> compare)
        {
            var pivot = array[((end - start) / 2) + start];

            int i = start - 1;
            int j = end + 1;

            while (true)
            {
                i++;

                while (compare(array[i], pivot) == -1)
                {
                    i++;
                }

                j--;

                while (compare(array[j], pivot) == 1)
                {
                    j--;
                }

                if (i >= j)
                {
                    return j;
                }

                Swap(ref array[i], ref array[j]);
            }
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            var temp = a;
            a = b;
            b = temp;
        }
    }
}
