﻿using System;
using QuickSort;

namespace JaggedArrays
{
    public static class ArrayExtension
    {
        /// <summary>
        /// Orders the rows in a jagged-array by ascending of the sum of the elements in them.
        /// </summary>
        /// <param name="source">The jagged-array to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown when source in null.</exception>
        public static void OrderByAscendingBySum(this int[][]? source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            source.QuickSort((a, b) => 
            {
                var sumA = a?.Sum() ?? int.MinValue;
                var lenA = a?.Length ?? 0;
                var sumB = b?.Sum() ?? int.MinValue;
                var lenB = b?.Length ?? 0;

                var result = sumA.CompareTo(sumB);
                return result != 0 ? result : lenA.CompareTo(lenB);
            });
        }
        
        /// <summary>
        /// Orders the rows in a jagged-array by descending of the sum of the elements in them.
        /// </summary>
        /// <param name="source">The jagged-array to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown when source in null.</exception>
        public static void OrderByDescendingBySum(this int[][]? source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            source.QuickSort((a, b) =>
            {
                var sumA = a?.Sum() ?? int.MinValue;
                var lenA = a?.Length ?? 0;
                var sumB = b?.Sum() ?? int.MinValue;
                var lenB = b?.Length ?? 0;

                var result = sumB.CompareTo(sumA);
                return result != 0 ? result : lenB.CompareTo(lenA);
            });
        }
        
        /// <summary>
        /// Orders the rows in a jagged-array by ascending of the max of the elements in them.
        /// </summary>
        /// <param name="source">The jagged-array to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown when source in null.</exception>
        public static void OrderByAscendingByMax(this int[][]? source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            source.QuickSort((a, b) =>
            {
                var maxA = a?.Max() ?? int.MinValue;
                var lenA = a?.Length ?? 0;
                var maxB = b?.Max() ?? int.MinValue;
                var lenB = b?.Length ?? 0;

                var result = maxA.CompareTo(maxB);
                return result != 0 ? result : lenA.CompareTo(lenB);
            });
        }
        
        /// <summary>
        /// Orders the rows in a jagged-array by descending of the max of the elements in them.
        /// </summary>
        /// <param name="source">The jagged-array to sort.</param>
        /// <exception cref="ArgumentNullException">Thrown when source in null.</exception>
        public static void OrderByDescendingByMax(this int[][]? source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            source.QuickSort((a, b) =>
            {
                var maxA = a?.Max() ?? int.MinValue;
                var lenA = a?.Length ?? 0;
                var maxB = b?.Max() ?? int.MinValue;
                var lenB = b?.Length ?? 0;

                var result = maxB.CompareTo(maxA);
                return result != 0 ? result : lenB.CompareTo(lenA);
            });
        }
    }
}
