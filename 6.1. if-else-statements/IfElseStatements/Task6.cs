﻿namespace IfStatements
{
    public static class Task6
    {
        public static int DoSomething(int i)
        {
            return i switch
            {
                < -8 => i * i,
                <= -2 => 3 * i,
                >= 7 => 2 * i,
                > 3 => -i * (i - 1),
                _ => (2 * i) + (i * i)
            };
        }
    }
}
