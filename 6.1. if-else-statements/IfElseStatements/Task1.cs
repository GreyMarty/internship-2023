﻿namespace IfStatements
{
    public static class Task1
    {
        public static int DoSomething(bool b)
        {
            return b ? 123 : -123;
        }
    }
}
