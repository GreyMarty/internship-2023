﻿namespace IfStatements
{
    public static class Task2
    {
        public static int DoSomething1(bool b1, bool b2)
        {
            if (b1)
            {
                return b2 ? 123 : -345;
            }
            else
            {
                return b2 ? -567 : 789;
            }
        }

        public static int DoSomething2(bool b1, bool b2)
        {
            return DoSomething1(b1, b2);
        }
    }
}
