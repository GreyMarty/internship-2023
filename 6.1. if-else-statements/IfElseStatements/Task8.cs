﻿namespace IfStatements
{
    public static class Task8
    {
        public static int DoSomething(int i1, int i2)
        {
            if (i1 >= -4 && i1 < 9)
            {
                return i2 switch
                {
                    > 5 => i1 + i2,
                    <= -3 => (2 * i1) - i2,
                    _ => i2 - (i1 * i1)
                };
            }
            else if (i1 < -4)
            {
                return i2 switch
                {
                    >= 7 => (2 * i2) - i1,
                    < -5 => i2 - i1,
                    _ => i1 - i2
                };
            }
            else if (i1 >= 9)
            {
                return i2 switch
                {
                    >= 7 => i1 - (i2 * i2),
                    < -7 => 2 * (i1 - i2),
                    _ => i1 * i2
                };
            }

            return 0;
        }
    }
}
