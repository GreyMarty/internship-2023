﻿namespace IfStatements
{
    public static class Task7
    {
        public static int DoSomething(bool b, int i)
        {
            if (b)
            {
                return i switch
                {
                    < -5 or >= 5 => i + 10,
                    >= -5 and < 5 => 10 - (i * i)
                };
            }
            else
            {
                return i switch
                {
                    <= -7 or > 4 => i - 100,
                    > -7 and <= 4 => 10 - i
                };
            }
        }
    }
}
