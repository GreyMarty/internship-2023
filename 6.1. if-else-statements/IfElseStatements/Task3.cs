﻿namespace IfStatements
{
    public static class Task3
    {
        public static int DoSomething1(bool b, int i)
        {
            if (b)
            {
                return i <= -6 ? i - 10 : i + 1;
            }
            else
            {
                return i < 8 ? i - 1 : i + 10;
            }
        }

        public static int DoSomething2(bool b, int i)
        {
            return DoSomething1(b, i);
        }
    }
}
