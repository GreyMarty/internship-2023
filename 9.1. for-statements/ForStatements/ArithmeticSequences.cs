﻿namespace ForStatements
{
    public static class ArithmeticSequences
    {
        public static int SumArithmeticSequenceTerms1(int a, int n, int d = 1)
        {
            int sum = 0;

            for (int i = 0; i < n; i++)
            {
                sum += a + (i * d);
            }

            return sum;
        }

        public static int SumArithmeticSequenceTerms2(int n)
        {
            return SumArithmeticSequenceTerms1(47, n, 13);
        }

        public static int SumArithmeticSequenceTerms3(int a, int n)
        {
            return SumArithmeticSequenceTerms1(a, n, 5);
        }
    }
}
