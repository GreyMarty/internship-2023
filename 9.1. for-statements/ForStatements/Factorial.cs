﻿namespace ForStatements
{
    public static class Factorial
    {
        public static int GetFactorial(int n)
        {
            int factorial = 1;

            for (int i = 2; i <= n; i++)
            {
                factorial *= i;
            }

            return factorial;
        }

        public static int SumFactorialDigits(int n)
        {
            int sum = 0;

            for (int factorial = GetFactorial(n); factorial > 0; factorial /= 10)
            {
                sum += factorial % 10;
            }

            return sum;
        }
    }
}
