﻿namespace ForStatements
{
    public static class FibonacciSequence
    {
        public static int GetFibonacciNumber(int n)
        {
            int i1 = 0;
            int i2 = 1;

            for (int i = 0; i < n; i++)
            {
                int temp = i1;
                i1 = i2;
                i2 += temp;
            }

            return i1;
        }

        public static ulong GetProductOfFibonacciNumberDigits(ulong n)
        {
            ulong product = n > 0 ? 1uL : 0uL;

            for (ulong number = (ulong)GetFibonacciNumber((int)n); number > 0; number /= 10)
            {
                product *= number % 10;
            }

            return product;
        }
    }
}
