﻿namespace ForStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            if (n <= 1)
            {
                return false;
            }

            if (n == 2)
            {
                return true;
            }

            if (n % 2 == 0)
            {
                return false;
            }

            for (uint i = 3; i * i <= n; i += 2)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static ulong SumDigitsOfPrimeNumbers(int start, int end)
        {
            ulong sum = 0;

            for (uint i = (uint)start; i <= end; i++)
            {
                if (IsPrimeNumber(i))
                {
                    for (uint j = i; j > 0; j /= 10)
                    {
                        sum += j % 10;
                    }
                }
            }

            return sum;
        }
    }
}
