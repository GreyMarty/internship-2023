﻿namespace ForStatements
{
    public static class GeometricSequences
    {
        public static ulong GetGeometricSequenceTermsProduct(uint a, uint r, uint n)
        {
            ulong product = 1;

            for (uint i = 0; i < n; i++)
            {
                product *= a * (ulong)Math.Pow(r, i);
            }

            return product;
        }

        public static ulong SumGeometricSequenceTerms(uint n)
        {
            ulong sum = 0;

            uint a = 5;
            uint r = 3;

            for (uint i = 0; i < n; i++)
            {
                sum += a * (ulong)Math.Pow(r, i);
            }

            return sum;
        }

        public static ulong CountGeometricSequenceTerms1(uint a, uint r, uint maxTerm)
        {
            ulong count = 0;

            for (ulong term = a; term <= maxTerm; term *= r)
            {
                count++;
            }

            return count;
        }

        public static ulong CountGeometricSequenceTerms2(uint a, uint r, uint n, uint minTerm)
        {
            ulong term = a;
            ulong count = 0;

            for (uint i = 0; i < n; i++)
            {
                if (term >= minTerm)
                {
                    count++;
                }

                term *= r;
            }

            return count;
        }
    }
}
