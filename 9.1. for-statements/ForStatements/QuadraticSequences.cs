﻿namespace ForStatements
{
    public static class QuadraticSequences
    {
        public static uint CountQuadraticSequenceTerms(long a, long b, long c, long maxTerm)
        {
            uint count = 0;
            long term = a + b + c;

            for (int i = 2; term <= maxTerm; i++)
            {
                term = (a * i * i) + (b * i) + c;
                count++;
            }

            return count;
        }

        public static ulong GetQuadraticSequenceTermsProduct1(uint count)
        {
            ulong product = count > 0u ? 1uL : 0uL;

            ulong a = 7;
            ulong b = 4;
            ulong c = 2;

            for (uint i = 1; i <= count; i++)
            {
                ulong term = (a * i * i) + (b * i) + c;
                product *= term;
            }

            return product;
        }

        public static ulong GetQuadraticSequenceProduct2(long a, long b, long c, long startN, long count)
        {
            ulong product = count > 0u ? 1uL : 0uL;
            long endN = startN + count;

            for (long i = startN; i < endN; i++)
            {
                ulong term = (ulong)((a * i * i) + (b * i) + c);
                product *= term;
            }

            return product;
        }
    }
}
