﻿using System;
using System.Text;

namespace SpiralMatrix
{
    /// <summary>
    /// Matrix manipulation.
    /// </summary>
    public static class MatrixExtension
    {
        /// <summary>
        /// Fills the matrix with natural numbers, starting from 1 in the top-left corner, increasing in an inward, clockwise spiral order.
        /// </summary>
        /// <param name="size">Matrix size.</param>
        /// <returns>Filled matrix.</returns>
        /// <exception cref="ArgumentException">Thrown when matrix size less or equal zero.</exception>
        /// <example> size = 3
        ///     1 2 3
        ///     8 9 4
        ///     7 6 5.
        /// </example>
        /// <example> size = 4
        ///     1  2  3  4
        ///     12 13 14 5
        ///     11 16 15 6
        ///     10 9  8  7.
        /// </example>
        public static int[,] GetMatrix(int size)
        {
            if (size <= 0)
            {
                throw new ArgumentException($"{nameof(size)} must be greater than zero.", nameof(size));
            }

            var matrix = new int[size, size];

            int y = 0;
            int x = 0;
            int n = 1;
            int length = size;
            int count = size * size;

            while (n <= count)
            {
                for (int i = 0; i < length && n <= count; i++)
                {
                    matrix[y, x++] = n++;
                }

                x--;
                y++;
                length--;

                for (int i = 0; i < length && n <= count; i++)
                {
                    matrix[y++, x] = n++;
                }

                y--;
                x--;

                for (int i = 0; i < length && n <= count; i++)
                {
                    matrix[y, x--] = n++;
                }

                x++;
                y--;
                length--;

                for (int i = 0; i < length && n <= count; i++)
                {
                    matrix[y--, x] = n++;
                }

                y++;
                x++;
            }

            return matrix;
        }
    }
}
