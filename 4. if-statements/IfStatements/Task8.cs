﻿namespace IfStatements
{
    public static class Task8
    {
        public static bool DoSomething(bool b, int i)
        {
            if (b)
            {
                return i switch
                {
                    (>= -3 and < 0) or < -6 => true,
                    (<= 3 and > 0) or > 6 => true,
                    _ => false
                };
            }
            else
            {
                return i switch
                {
                    0 => false,
                    (<= -3 and >= -6) or(>= 3 and <= 6) => false,
                    _ => true
                };
            }
        }
    }
}
