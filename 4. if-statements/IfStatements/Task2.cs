﻿namespace IfStatements
{
    public static class Task2
    {
        public static int DoSomething1(int i)
        {
            return i switch
            {
                < -5 => -i * i,
                < 0 => -i,
                _ => i
            };
        }

        public static int DoSomething2(int i)
        {
            return DoSomething1(i);
        }
    }
}
