﻿namespace IfStatements
{
    public static class Task13
    {
        public static int DoSomething(bool b, int i)
        {
            if (b)
            {
                return i switch
                {
                    < -8 => i + 5,
                    < -4 => i,
                    < 0 => i + 5,
                    0 => 10,
                    <= 3 => i - 5,
                    _ => -i
                };
            }

            return i switch
            {
                <= -5 => -i,
                <= 5 => 10 - i,
                _ => -i
            };
        }
    }
}
