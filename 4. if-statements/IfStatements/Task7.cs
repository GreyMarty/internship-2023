﻿namespace IfStatements
{
    public static class Task7
    {
        public static int DoSomething(bool b, int i)
        {
            return (b, i) switch
            {
                (true, > -7 and < 7) => 7 - i,
                (false, >= 5 or <= -5) => i + 5,
                _ => i
            };
        }
    }
}
