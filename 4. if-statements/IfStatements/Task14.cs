﻿namespace IfStatements
{
    public static class Task14
    {
        public static int DoSomething(bool b1, bool b2, int i)
        {
            if (b1 && b2)
            {
                return i switch
                {
                    <= -5 => 10 - (2 * i),
                    <= 5 => -2 * i,
                    _ => 10 - (2 * i)
                };
            }

            if (b1 && !b2)
            {
                return i switch
                {
                    <= -5 => i * i * i,
                    <= 5 => i * i,
                    _ => i * i * i
                };
            }

            if (!b1 && b2)
            {
                return i switch
                {
                    < -9 => -i,
                    < -7 => i,
                    < -3 => 10 * i,
                    <= 7 => i,
                    _ => -i
                };
            }

            return i switch
            {
                < -9 => -i,
                < -3 => i,
                < 0 => -100 * i,
                0 => 0,
                < 5 => -100 * i,
                <= 7 => i,
                _ => -i
            };
        }
    }
}
