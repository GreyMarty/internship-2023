﻿namespace IfStatements
{
    public static class Task6
    {
        public static int DoSomething(int i)
        {
            return i switch
            {
                < 0 and >= -3 => 3 * i,
                > 0 and <= 3 => i * (1 - i),
                _ => i
            };
        }
    }
}
