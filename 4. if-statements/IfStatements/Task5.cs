﻿namespace IfStatements
{
    public static class Task5
    {
        public static int DoSomething(int i)
        {
            return i + i switch
            {
                < 0 and >= -5 => 5,
                > 0 and <= 5 => -5,
                _ => 0
            };
        }
    }
}
