﻿namespace IfStatements
{
    public static class Task1
    {
        public static int DoSomething(int i)
        {
            return i > 0 ? i : 0;
        }
    }
}
