﻿namespace IfStatements
{
    public static class Task12
    {
        public static int DoSomething(int i)
        {
            return i switch
            {
                < -8 => i * i,
                < -5 => i,
                < 5 => (i * i) - i,
                < 10 => i,
                _ => -i * i
            };
        }
    }
}
