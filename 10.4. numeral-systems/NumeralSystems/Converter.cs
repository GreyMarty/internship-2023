﻿using System.Globalization;
using System.Text;

namespace NumeralSystems
{
    public static class Converter
    {
        /// <summary>
        /// Gets the value of a positive integer to its equivalent string representation in the octal numeral systems.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>The equivalent string representation of the number in the octal numeral systems.</returns>
        /// <exception cref="ArgumentException">Thrown if number is less than zero.</exception>
        public static string GetPositiveOctal(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} should be greater or equal to 0.", nameof(number));
            }

            var builder = new StringBuilder();

            int @base = 8;

            while (number > 0)
            {
                builder.Insert(0, number % @base);
                number /= @base;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gets the value of a positive integer to its equivalent string representation in the decimal numeral systems.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>The equivalent string representation of the number in the decimal numeral systems.</returns>
        /// <exception cref="ArgumentException">Thrown if number is less than zero.</exception>
        public static string GetPositiveDecimal(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} should be greater or equal to 0.", nameof(number));
            }

            var builder = new StringBuilder();

            int @base = 10;

            while (number > 0)
            {
                builder.Insert(0, number % @base);
                number /= @base;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gets the value of a positive integer to its equivalent string representation in the hexadecimal numeral systems.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>The equivalent string representation of the number in the hexadecimal numeral systems.</returns>
        /// <exception cref="ArgumentException">Thrown if number is less than zero.</exception>
        public static string GetPositiveHex(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} should be greater or equal to 0.", nameof(number));
            }

            var builder = new StringBuilder();

            int @base = 16;

            while (number > 0)
            {
                var value = number % @base;
                var chr = (char)(value < 10 ? '0' + value : 'A' + value - 10);

                builder.Insert(0, chr);
                number /= @base;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gets the value of a positive integer to its equivalent string representation in a specified radix.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <param name="radix">Base of the numeral systems.</param>
        /// <returns>The equivalent string representation of the number in a specified radix.</returns>
        /// <exception cref="ArgumentException">Thrown if radix is not equal 8, 10 or 16.</exception>
        /// <exception cref="ArgumentException">Thrown if number is less than zero.</exception>
        public static string GetPositiveRadix(this int number, int radix)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} should be greater or equal to 0.", nameof(number));
            }

            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.");
            }

            var builder = new StringBuilder();

            while (number > 0)
            {
                var value = number % radix;
                var chr = (char)(value < 10 ? '0' + value : 'A' + value - 10);

                builder.Insert(0, chr);
                number /= radix;
            }

            return builder.ToString();
        }

        public static string GetPositiveRadix(this long number, int radix)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} should be greater or equal to 0.", nameof(number));
            }

            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.");
            }

            var builder = new StringBuilder();

            while (number > 0)
            {
                var value = number % radix;
                var chr = (char)(value < 10 ? '0' + value : 'A' + value - 10);

                builder.Insert(0, chr);
                number /= radix;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gets the value of a signed integer to its equivalent string representation in a specified radix.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <param name="radix">Base of the numeral systems.</param>
        /// <returns>The equivalent string representation of the number in a specified radix.</returns>
        /// <exception cref="ArgumentException">Thrown if radix is not equal 8, 10 or 16.</exception>
        public static string GetRadix(this int number, int radix)
        {
            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.");
            }

            if (number < 0)
            {
                if (radix == 10)
                {
                    return "-" + GetPositiveRadix(number, radix);
                }
                else
                {
                    var longNumber = (long)Math.Pow(2, 32) + number;
                    return GetPositiveRadix(longNumber, radix);
                }
            }

            return GetPositiveRadix(number, radix);
        }
    }
}
