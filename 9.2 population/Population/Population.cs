﻿namespace PopulationTask
{
    public static class Population
    {
        /// <summary>
        /// Calculates the count of years which the town need to see its population greater or equal to currentPopulation inhabitants.
        /// </summary>
        /// <param name="initialPopulation">The population at the beginning of a year.</param>
        /// <param name="percent">The percentage of growth per year.</param>
        /// <param name="visitors">The visitors (new inhabitants per year) who come to live in the town.</param>
        /// <param name="currentPopulation">The population at present.</param>
        /// <returns>The count of years which the town need to see its population greater or equal to currentPopulation inhabitants.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown when initial population is less or equals 0
        /// - or -
        /// the count of visitors cannot be less 0
        /// - or -
        /// the current population is less or equals 0
        /// - or -
        /// the current population is less than initial population.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">Throw if the value of percents is less then 0% or more then 100%.</exception>
        public static int GetYears(int initialPopulation, double percent, int visitors, int currentPopulation)
        {
            if (initialPopulation <= 0)
            {
                throw new ArgumentException($"Initial population cannot be less or equal to zero.", nameof(initialPopulation));
            }

            if (currentPopulation <= 0)
            {
                throw new ArgumentException($"Current population cannot be less or equal to zero.", nameof(initialPopulation));
            }

            if (currentPopulation < initialPopulation)
            {
                throw new ArgumentException($"Current population cannot be less than initial population.", nameof(initialPopulation));
            }

            if (visitors < 0)
            {
                throw new ArgumentException("Count of visitors cannot be less zero.", nameof(visitors));
            }

            if (percent < 0 || percent > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(percent), "Value of percents cannot be less then 0% or more then 100%.");
            }

            int i;
            int population = initialPopulation;

            for (i = 0; population < currentPopulation; i++)
            {
                population = (int)(population * (1 + (percent / 100.0))) + visitors;
            }

            return i;
        }
    }
}
