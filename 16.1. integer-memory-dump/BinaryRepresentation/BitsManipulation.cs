﻿using System;
using System.Text;

namespace BinaryRepresentation
{
    public static class BitsManipulation
    {
        /// <summary>
        /// Get binary memory representation of signed long integer.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>Binary memory representation of signed long integer.</returns>
        public static string GetMemoryDumpOf(long number)
        {
            var builder = new StringBuilder();
            var n = sizeof(long) * 8;

            for (int i = n - 1; i >= 0; i--)
            {
                builder.Append(((ulong)number & (1uL << i)) >> i);
            }

            return builder.ToString();
        }
    }
}
