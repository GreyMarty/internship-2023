﻿using System.Globalization;

namespace TransformerToWords
{
    /// <summary>
    /// Implements transformer class.
    /// </summary>
    public class Transformer
    {
        /// <summary>
        /// Converts number's digital representation into words.
        /// </summary>
        /// <param name="number">Number to convert.</param>
        /// <returns>Words representation.</returns>
        public static string TransformToWords(double number)
        {
            if (double.IsNaN(number))
            {
                return "Not a Number";
            }

            if (double.IsNegativeInfinity(number))
            {
                return "Negative Infinity";
            }

            if (double.IsInfinity(number))
            {
                return "Positive Infinity";
            }

            if (number == double.Epsilon)
            {
                return "Double Epsilon";
            }

            var literals = new Dictionary<char, string>()
            {
                { '+', "plus" },
                { '-', "minus" },
                { '.', "point" },
                { 'E', "E" },
                { 'e', "E" },
                { '0', "zero" },
                { '1', "one" },
                { '2', "two" },
                { '3', "three" },
                { '4', "four" },
                { '5', "five" },
                { '6', "six" },
                { '7', "seven" },
                { '8', "eight" },
                { '9', "nine" },
            };

            var words = new List<string>();

            var culture = new CultureInfo("en-US");

            foreach (var chr in number.ToString(null, culture))
            {
                words.Add(literals[chr]);
            }

            words[0] = char.ToUpper(words[0][0], culture) + words[0][1..];
            return string.Join(' ', words);
        }

        /// <summary>
        /// Transforms each element of source array into its 'word format'.
        /// </summary>
        /// <param name="source">Source array.</param>
        /// <returns>Array of 'word format' of elements of source array.</returns>
        /// <exception cref="ArgumentNullException">Thrown when array is null.</exception>
        /// <exception cref="ArgumentException">Thrown when array is empty.</exception>
        /// <example>
        /// new[] { 2.345, -0.0d, 0.0d, 0.1d } => { "Two point three four five", "Minus zero", "Zero", "Zero point one" }.
        /// </example>
#pragma warning disable CA1822 // Mark members as static
        public string[] Transform(double[]? source)
#pragma warning restore CA1822 // Mark members as static
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException($"{nameof(source)} is empty.", nameof(source));
            }

            return source
                .Select(x => TransformToWords(x))
                .ToArray();
        }
    }
}
