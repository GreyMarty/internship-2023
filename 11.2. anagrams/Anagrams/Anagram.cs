﻿using System;
using System.Globalization;

namespace Anagrams
{
    public class Anagram
    {
        private readonly Dictionary<char, uint> _letterCounts;

        public string Source { get; init; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Anagram"/> class.
        /// </summary>
        /// <param name="sourceWord">Source word.</param>
        /// <exception cref="ArgumentNullException">Thrown when source word is null.</exception>
        /// <exception cref="ArgumentException">Thrown when  source word is empty.</exception>
        public Anagram(string? sourceWord)
        {
            if (sourceWord is null)
            {
                throw new ArgumentNullException(nameof(sourceWord));
            }

            if (sourceWord.Length == 0)
            {
                throw new ArgumentException($"{nameof(sourceWord)} must not be empty.", nameof(sourceWord));
            }

            Source = sourceWord.ToLower(CultureInfo.InvariantCulture);
            _letterCounts = new();

            foreach (var chr in Source)
            {
                if (chr == ' ')
                {
                    continue;
                }

                if (_letterCounts.ContainsKey(chr))
                {
                    _letterCounts[chr]++;
                }
                else
                {
                    _letterCounts[chr] = 1;
                }
            }
        }

        /// <summary>
        /// From the list of possible anagrams selects the correct subset.
        /// </summary>
        /// <param name="candidates">A list of possible anagrams.</param>
        /// <returns>The correct sublist of anagrams.</returns>
        /// <exception cref="ArgumentNullException">Thrown when candidates list is null.</exception>
        public string[] FindAnagrams(string[]? candidates)
        {
            if (candidates is null)
            {
                throw new ArgumentNullException(nameof(candidates));
            }

            List<string> result = new List<string>();

            foreach (var candidate in candidates)
            {
                if (IsAnagram(candidate))
                {
                    result.Add(candidate);
                }
            }

            return result.ToArray();

            throw new NotImplementedException("You need to implement this function.");
        }

        public bool IsAnagram(string candidate)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                throw new ArgumentNullException(nameof(candidate));
            }

            candidate = candidate.ToLower(CultureInfo.InvariantCulture);

            if (candidate == Source)
            {
                return false;
            }

            var letterCounts = new Dictionary<char, uint>(_letterCounts);

            foreach (var chr in candidate)
            {
                if (chr == ' ')
                {
                    continue;
                }

                var lowerChr = char.ToLower(chr, CultureInfo.InvariantCulture);

                if (!letterCounts.ContainsKey(lowerChr))
                {
                    return false;
                }

                if (letterCounts[lowerChr]-- <= 0)
                {
                    return false;
                }
            }

            uint remains = 0;

            foreach (var count in letterCounts.Values)
            {
                remains += count;
            }

            return remains == 0;
        }
    }
}
