﻿using System.Text.RegularExpressions;

namespace IsbnValidator
{
    public static class Validator
    {
        /// <summary>
        /// Returns true if the specified <paramref name="isbn"/> is valid; returns false otherwise.
        /// </summary>
        /// <param name="isbn">The string representation of 10-digit ISBN.</param>
        /// <returns>true if the specified <paramref name="isbn"/> is valid; false otherwise.</returns>
        /// <exception cref="ArgumentException"><paramref name="isbn"/> is empty or has only white-space characters.</exception>
        public static bool IsIsbnValid(string isbn)
        {
            const char Separator = '-';
            const uint MaxIndex = 10;

            if (string.IsNullOrWhiteSpace(isbn))
            {
                throw new ArgumentException("Source string cannot be null or empty or whitespace.", nameof(isbn));
            }

            if (isbn.Length < 10 || isbn.Length > 13)
            {
                return false;
            }

            string pattern = @"^([\d]+-?)+([\dX])$";

            if (!Regex.IsMatch(isbn, pattern))
            {
                return false;
            }

            int index = 1;
            int checksum = 0;

            foreach (var chr in isbn)
            {
                if (index > MaxIndex)
                {
                    return false;
                }

                if (chr != Separator)
                {
                    int value = chr == 'X' ? 10 : chr - '0';
                    checksum += value * index;
                    index++;
                }
            }

            return index == MaxIndex + 1 && checksum % 11 == 0;
        }
    }
}
