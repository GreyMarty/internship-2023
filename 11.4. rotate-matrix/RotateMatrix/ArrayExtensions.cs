﻿namespace RotateMatrix;

public static class ArrayExtensions
{
    /// <summary>
    /// Rotates the image clockwise by 90° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate90DegreesClockwise(this int[,]? matrix)
    {
        if (matrix is null)
        {
            throw new ArgumentNullException(nameof(matrix));
        }

        int height = matrix.GetLength(0);
        int width = matrix.GetLength(1);

        var copy = (int[,])matrix.Clone();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int x1 = height - y - 1;
                int y1 = x;

                matrix[y1, x1] = copy[y, x];
            }
        }
    }

    /// <summary>
    /// Rotates the image counterclockwise by 90° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate90DegreesCounterClockwise(this int[,]? matrix)
    {
        if (matrix is null)
        {
            throw new ArgumentNullException(nameof(matrix));
        }

        int height = matrix.GetLength(0);
        int width = matrix.GetLength(1);

        var copy = (int[,])matrix.Clone();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int x1 = y;
                int y1 = width - x - 1;

                matrix[y1, x1] = copy[y, x];
            }
        }
    }

    /// <summary>
    /// Rotates the image clockwise by 180° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate180DegreesClockwise(this int[,]? matrix)
    {
        if (matrix is null)
        {
            throw new ArgumentNullException(nameof(matrix));
        }

        int height = matrix.GetLength(0);
        int width = matrix.GetLength(1);

        var copy = (int[,])matrix.Clone();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int x1 = width - x - 1;
                int y1 = height - y - 1;

                matrix[y1, x1] = copy[y, x];
            }
        }
    }

    /// <summary>
    /// Rotates the image counterclockwise by 180° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate180DegreesCounterClockwise(this int[,]? matrix)
    {
        Rotate180DegreesClockwise(matrix);
    }

    /// <summary>
    /// Rotates the image clockwise by 270° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate270DegreesClockwise(this int[,]? matrix)
    {
        Rotate90DegreesCounterClockwise(matrix);
    }

    /// <summary>
    /// Rotates the image counterclockwise by 270° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate270DegreesCounterClockwise(this int[,]? matrix)
    {
        Rotate90DegreesClockwise(matrix);
    }

    /// <summary>
    /// Rotates the image clockwise by 360° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate360DegreesClockwise(this int[,]? matrix)
    {
        if (matrix is null)
        {
            throw new ArgumentNullException(nameof(matrix));
        }
    }

    /// <summary>
    /// Rotates the image counterclockwise by 360° in place.
    /// </summary>
    /// <param name="matrix">Two-dimension square matrix that presents an image.</param>
    /// <exception cref="ArgumentNullException">Throw when source matrix is null.</exception>
    public static void Rotate360DegreesCounterClockwise(this int[,]? matrix)
    {
        if (matrix is null)
        {
            throw new ArgumentNullException(nameof(matrix));
        }
    }
}
