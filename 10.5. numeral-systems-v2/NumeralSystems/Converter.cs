﻿namespace NumeralSystems
{
    /// <summary>
    /// Converts a string representations of a numbers to its integer equivalent.
    /// </summary>
    public static class Converter
    {
        /// <summary>
        /// Converts the string representation of a positive number in the octal numeral system to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the octal numeral system.</param>
        /// <returns>A positive decimal value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if source string presents a negative number
        /// - or
        /// contains invalid symbols (non-octal alphabetic characters).
        /// Valid octal alphabetic characters: 0,1,2,3,4,5,6,7.
        /// </exception>
        public static int ParsePositiveFromOctal(this string source)
        {
            var value = ParseFromOctal(source);
            return value >= 0 ? value : throw new ArgumentException($"{nameof(source)} does not represent a positive number in the octal numeral system.", nameof(source));
        }

        public static int ParseFromOctal(this string source)
        {
            var value = 0uL;

            foreach (var chr in source)
            {
                if (chr < '0' || chr >= '8')
                {
                    throw new ArgumentException($"{nameof(source)} contains invalid characters.", nameof(source));
                }

                value = (value << 3) | (byte)(chr - '0');
            }

            return (int)value;
        }

        /// <summary>
        /// Converts the string representation of a positive number in the decimal numeral system to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the decimal numeral system.</param>
        /// <returns>A positive decimal value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if source string presents a negative number
        /// - or
        /// contains invalid symbols (non-decimal alphabetic characters).
        /// Valid decimal alphabetic characters: 0,1,2,3,4,5,6,7,8,9.
        /// </exception>
        public static int ParsePositiveFromDecimal(this string source)
        {
            var value = ParseFromDecimal(source);
            return value >= 0
                ? value
                : throw new ArgumentException($"{nameof(source)} does not represent a positive number in the decimal numeral system.", nameof(source));
        }

        public static int ParseFromDecimal(this string source)
        {
            var value = 0L;
            var sign = source[0] == '-' ? -1 : 1;

            if (sign < 0)
            {
                source = source.Substring(1);
            }

            foreach (var chr in source)
            {
                if (chr < '0' || chr > '9')
                {
                    throw new ArgumentException($"{nameof(source)} contains invalid characters.", nameof(source));
                }

                value *= 10;
                value += chr - '0';
            }

            if (value > int.MaxValue)
            {
                throw new ArgumentException($"{nameof(source)} is too big.", nameof(source));
            }

            return (int)value * sign;
        }

        /// <summary>
        /// Converts the string representation of a positive number in the hex numeral system to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the hex numeral system.</param>
        /// <returns>A positive decimal value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if source string presents a negative number
        /// - or
        /// contains invalid symbols (non-hex alphabetic characters).
        /// Valid hex alphabetic characters: 0,1,2,3,4,5,6,7,8,9,A(or a),B(or b),C(or c),D(or d),E(or e),F(or f).
        /// </exception>
        public static int ParsePositiveFromHex(this string source)
        {
            var value = ParseFromHex(source);
            return value >= 0
                ? value
                : throw new ArgumentException($"{nameof(source)} does not represent a positive number in the hex numeral system.", nameof(source));
        }

        public static int ParseFromHex(this string source)
        {
            var value = 0uL;

            foreach (var chr in source)
            {
                value <<= 4;

                if (chr >= '0' && chr <= '9')
                {
                    value |= (byte)(chr - '0');
                }
                else if (chr >= 'A' && chr <= 'F')
                {
                    value |= (byte)(10 + chr - 'A');
                }
                else if (chr >= 'a' && chr <= 'f')
                {
                    value |= (byte)(10 + chr - 'a');
                }
                else
                {
                    throw new ArgumentException($"{nameof(source)} contains invalid characters.", nameof(source));
                }
            }

            return (int)value;
        }

        /// <summary>
        /// Converts the string representation of a positive number in the octal, decimal or hex numeral system to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the the octal, decimal or hex numeral system.</param>
        /// <param name="radix">The radix.</param>
        /// <returns>A positive decimal value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if source string presents a negative number
        /// - or
        /// contains invalid for given numeral system symbols
        /// -or-
        /// the radix is not equal 8, 10 or 16.
        /// </exception>
        public static int ParsePositiveByRadix(this string source, int radix)
        {
            var value = ParseByRadix(source, radix);
            return value >= 0
                ? value
                : throw new ArgumentException($"{nameof(source)} does not represent a positive number in numeral system.", nameof(source));
        }

        /// <summary>
        /// Converts the string representation of a signed number in the octal, decimal or hex numeral system to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="source">The string representation of a signed number in the the octal, decimal or hex numeral system.</param>
        /// <param name="radix">The radix.</param>
        /// <returns>A signed decimal value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown if source contains invalid for given numeral system symbols
        /// -or-
        /// the radix is not equal 8, 10 or 16.
        /// </exception>
        public static int ParseByRadix(this string source, int radix)
        {
            return radix switch
            {
                8 => ParseFromOctal(source),
                10 => ParseFromDecimal(source),
                16 => ParseFromHex(source),
                _ => throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.", nameof(radix))
            };
        }

        /// <summary>
        /// Converts the string representation of a positive number in the octal numeral system to its 32-bit signed integer equivalent.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the octal numeral system.</param>
        /// <param name="value">A positive decimal value.</param>
        /// <returns>true if s was converted successfully; otherwise, false.</returns>
        public static bool TryParsePositiveFromOctal(this string source, out int value)
        {
            try
            {
                value = ParsePositiveFromOctal(source);
                return true;
            }
            catch (ArgumentException)
            {
                value = default;
                return false;
            }
        }

        /// <summary>
        /// Converts the string representation of a positive number in the decimal numeral system to its 32-bit signed integer equivalent.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the decimal numeral system.</param>
        /// <returns>A positive decimal value.</returns>
        /// <param name="value">A positive decimal value.</param>
        /// <returns>true if s was converted successfully; otherwise, false.</returns>
        public static bool TryParsePositiveFromDecimal(this string source, out int value)
        {
            try
            {
                value = ParsePositiveFromDecimal(source);
                return true;
            }
            catch (ArgumentException)
            {
                value = default;
                return false;
            }
        }

        /// <summary>
        /// Converts the string representation of a positive number in the hex numeral system to its 32-bit signed integer equivalent.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the hex numeral system.</param>
        /// <returns>A positive decimal value.</returns>
        /// <param name="value">A positive decimal value.</param>
        /// <returns>true if s was converted successfully; otherwise, false.</returns>
        public static bool TryParsePositiveFromHex(this string source, out int value)
        {
            try
            {
                value = ParsePositiveFromHex(source);
                return true;
            }
            catch (ArgumentException)
            {
                value = default;
                return false;
            }
        }

        /// <summary>
        /// Converts the string representation of a positive number in the octal, decimal or hex numeral system to its 32-bit signed integer equivalent.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="source">The string representation of a positive number in the the octal, decimal or hex numeral system.</param>
        /// <param name="radix">The radix.</param>
        /// <returns>A positive decimal value.</returns>
        /// <param name="value">A positive decimal value.</param>
        /// <returns>true if s was converted successfully; otherwise, false.</returns>
        /// <exception cref="ArgumentException">Thrown the radix is not equal 8, 10 or 16.</exception>
        public static bool TryParsePositiveByRadix(this string source, int radix, out int value)
        {
            return radix switch
            {
                8 => TryParsePositiveFromOctal(source, out value),
                10 => TryParsePositiveFromDecimal(source, out value),
                16 => TryParsePositiveFromHex(source, out value),
                _ => throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.", nameof(radix))
            };
        }

        /// <summary>
        /// Converts the string representation of a signed number in the octal, decimal or hex numeral system to its 32-bit signed integer equivalent.
        /// A return value indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="source">The string representation of a signed number in the the octal, decimal or hex numeral system.</param>
        /// <param name="radix">The radix.</param>
        /// <returns>A positive decimal value.</returns>
        /// <param name="value">A positive decimal value.</param>
        /// <returns>true if s was converted successfully; otherwise, false.</returns>
        /// <exception cref="ArgumentException">Thrown the radix is not equal 8, 10 or 16.</exception>
        public static bool TryParseByRadix(this string source, int radix, out int value)
        {
            if (radix != 8 && radix != 10 && radix != 16)
            {
                throw new ArgumentException($"{nameof(radix)} is 8, 10 and 16 only.", nameof(radix));
            }

            try
            {
                value = ParseByRadix(source, radix);
                return true;
            }
            catch (ArgumentException)
            {
                value = default;
                return false;
            }
        }
    }
}
