﻿namespace LookingForArrayElements;

internal static class Util
{
    public static bool Any<T>(this T[] source, Func<T, bool> predicate, int startIndex)
    {
        if (startIndex >= source.Length)
        {
            return false;
        }

        if (predicate(source[startIndex]))
        {
            return true;
        }

        return source.Any(predicate, startIndex + 1);
    }
}
