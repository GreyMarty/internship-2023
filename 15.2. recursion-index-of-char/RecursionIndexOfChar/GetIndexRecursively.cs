﻿using System;

namespace RecursionIndexOfChar
{
    public static class GetIndexRecursively
    {
        public static int GetIndexOfChar(string? str, char value)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            return GetIndexOfChar(str, value, 0, str.Length);
        }

        public static int GetIndexOfChar(string? str, char value, int startIndex, int count)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (count <= 0)
            {
                return -1;
            }

            if (str[startIndex] == value)
            {
                return startIndex;
            }

            return GetIndexOfChar(str, value, startIndex + 1, count - 1);
        }
    }
}
