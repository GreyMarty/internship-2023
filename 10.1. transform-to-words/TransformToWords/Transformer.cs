﻿using System;
using System.Globalization;

namespace TransformToWords
{
    /// <summary>
    /// Provides transformer methods.
    /// </summary>
    public static class Transformer
    {
        /// <summary>
        /// Converts number's digital representation into words.
        /// </summary>
        /// <param name="number">Number to convert.</param>
        /// <returns>Words representation.</returns>
        public static string TransformToWords(double number)
        {
            if (double.IsNaN(number))
            {
                return "NaN";
            }

            if (double.IsNegativeInfinity(number))
            {
                return "Negative Infinity";
            }

            if (double.IsInfinity(number))
            {
                return "Positive Infinity";
            }

            if (number == double.Epsilon)
            {
                return "Double Epsilon";
            }

            var literals = new Dictionary<char, string>()
            {
                { '+', "plus" },
                { '-', "minus" },
                { '.', "point" },
                { 'E', "E" },
                { 'e', "E" },
                { '0', "zero" },
                { '1', "one" },
                { '2', "two" },
                { '3', "three" },
                { '4', "four" },
                { '5', "five" },
                { '6', "six" },
                { '7', "seven" },
                { '8', "eight" },
                { '9', "nine" },
            };

            var words = new List<string>();

            var culture = new CultureInfo("en-US");

            foreach (var chr in number.ToString(null, culture))
            {
                words.Add(literals[chr]);
            }

            words[0] = char.ToUpper(words[0][0], culture) + words[0][1..];
            return string.Join(' ', words);
        }
    }
}
