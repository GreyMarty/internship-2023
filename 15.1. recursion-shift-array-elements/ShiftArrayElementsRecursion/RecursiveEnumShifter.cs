﻿using ShiftArrayElementsRecursion;

namespace ShiftArrayElements
{
    public static class RecursiveEnumShifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using directions from <see cref="directions"/> array, one element shift per each direction array element.
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="directions">An array with directions.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">directions array is null.</exception>
        /// <exception cref="InvalidOperationException">direction array contains an element that is not <see cref="Direction.Left"/> or <see cref="Direction.Right"/>.</exception>
        public static int[] Shift(int[]? source, Direction[]? directions)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions is null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            if (directions.Length == 0)
            {
                return source;
            }

            var result = (int[])source.Clone();
            Shift(result, directions, 0);

            return result;
        }

        private static void Shift(int[] source, Direction[] directions, int iteration)
        {
            if (iteration >= directions.Length)
            {
                return;
            }

            if (source.Length < 2)
            {
                return;
            }

            var shift = directions[iteration] switch
            {
                Direction.Left => -1,
                Direction.Right => 1,
                _ => throw new InvalidOperationException()
            };

            Util.ShiftInPlace(source, shift);

            Shift(source, directions, iteration + 1);
        }
    }
}
