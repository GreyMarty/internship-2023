﻿using ShiftArrayElementsRecursion;

namespace ShiftArrayElements
{
    public static class RecursiveShifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (odd elements - left direction, even elements - right direction).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        public static int[] Shift(int[]? source, int[]? iterations)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations is null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            if (iterations.Length == 0)
            {
                return source;
            }

            var result = (int[])source.Clone();
            Shift(result, iterations, 0);

            return result;
        }

        private static void Shift(int[] source, int[] iterations, int iteration)
        {
            if (iteration >= iterations.Length)
            {
                return;
            }

            if (source.Length < 2)
            {
                return;
            }

            var direction = (iteration + 1) % 2 == 0 ? 1 : -1;
            Util.ShiftInPlace(source, direction * iterations[iteration]);

            Shift(source, iterations, iteration + 1);
        }
    }
}
