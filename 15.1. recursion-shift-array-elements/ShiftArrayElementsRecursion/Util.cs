﻿namespace ShiftArrayElementsRecursion;

internal static class Util
{
    public static void ShiftInPlace(int[] source, int shift)
    {
        var length = source.Length;

        shift = shift > 0
            ? shift % length
            : length - (-shift % length);

        Reverse(source, 0, length);
        Reverse(source, 0, shift);
        Reverse(source, shift, length - shift);
    }

    public static void Reverse(int[] source, int offset, int count)
    {
        for (int i = 0; i < count / 2; i++)
        {
            Swap(ref source[offset + i], ref source[offset + count - i - 1]);
        }
    }

    public static void Swap<T>(ref T a, ref T b)
    {
        var temp = a;
        a = b;
        b = temp;
    }
}
