﻿using System;
using System.Runtime.Serialization.Formatters;
using System.Security.AccessControl;
using System.Text;

namespace MorseCodeTranslator
{
    public static class Translator
    {
        public static string TranslateToMorse(string? message)
        {
            if (message is null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            var codes = new List<string>();

            for (int i = 0; i < message.Length; i++)
            {
                var codeRow = MorseCodes.CodeTable.FirstOrDefault(r => r[0] == char.ToUpperInvariant(message[i]));

                if (codeRow is null)
                {
                    continue;
                }

                codes.Add(string.Join(string.Empty, codeRow[1..]));
            }

            return string.Join(' ', codes);
        }

        public static string TranslateToText(string? morseMessage)
        {
            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage));
            }

            if (string.IsNullOrWhiteSpace(morseMessage))
            {
                return string.Empty;
            }

            var codes = MorseCodes.CodeTable.ToDictionary(
                r => string.Join(string.Empty, r[1..]),
                r => r[0]);

            var resultBuilder = new StringBuilder();
            var codeBuilder = new StringBuilder();

            foreach (var chr in morseMessage)
            {
                if (chr != ' ')
                {
                    codeBuilder.Append(chr);
                }
                else
                {
                    resultBuilder.Append(codes[codeBuilder.ToString()]);
                    codeBuilder.Clear();
                }
            }

            resultBuilder.Append(codes[codeBuilder.ToString()]);

            return resultBuilder.ToString();
        }

        public static void WriteMorse(char[][]? codeTable, string message, StringBuilder? morseMessageBuilder, char dot = '.', char dash = '-', char separator = ' ')
        {
            if (codeTable is null)
            {
                throw new ArgumentNullException(nameof(codeTable));
            }

            if (message is null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            if (morseMessageBuilder is null)
            {
                throw new ArgumentNullException(nameof(morseMessageBuilder));
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }

            for (int i = 0; i < message.Length; i++)
            {
                var codeRow = codeTable.FirstOrDefault(r => r[0] == char.ToUpperInvariant(message[i]));

                if (codeRow is null)
                {
                    continue;
                }

                var code = string.Join(string.Empty, codeRow[1..]).Replace('.', dot).Replace('-', dash);
                morseMessageBuilder.Append(code);
                morseMessageBuilder.Append(separator);
            }

            morseMessageBuilder.Length--;
        }

        public static void WriteText(char[][]? codeTable, string? morseMessage, StringBuilder? messageBuilder, char dot = '.', char dash = '-', char separator = ' ')
        {
            if (codeTable is null)
            {
                throw new ArgumentNullException(nameof(codeTable));
            }

            if (morseMessage is null)
            {
                throw new ArgumentNullException(nameof(morseMessage));
            }

            if (messageBuilder is null)
            {
                throw new ArgumentNullException(nameof(messageBuilder));
            }

            if (string.IsNullOrWhiteSpace(morseMessage))
            {
                return;
            }

            var codes = codeTable.ToDictionary(
                r => string.Join(string.Empty, r[1..]).Replace('.', dot).Replace('-', dash),
                r => r[0]);

            var codeBuilder = new StringBuilder();

            foreach (var chr in morseMessage)
            {
                if (chr != separator)
                {
                    codeBuilder.Append(chr);
                }
                else
                {
                    messageBuilder.Append(codes[codeBuilder.ToString()]);
                    codeBuilder.Clear();
                }
            }

            messageBuilder.Append(codes[codeBuilder.ToString()]);
        }
    }
}
