﻿namespace WhileStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            if (n == 1)
            {
                return false;
            }

            if (n == 2)
            {
                return true;
            }

            if ((n & 1) == 0)
            {
                return false;
            }

            uint i = 3;

            while (i * i <= n)
            {
                if (n % i == 0)
                {
                    return false;
                }

                i++;
            }

            return true;
        }

        public static uint GetLastPrimeNumber(uint n)
        {
            while (n > 0)
            {
                if (IsPrimeNumber(n))
                {
                    return n;
                }

                n--;
            }

            return 0;
        }

        public static uint SumLastPrimeNumbers(uint n, uint count)
        {
            uint sum = 0;

            while (n > 0 && count > 0)
            {
                if (IsPrimeNumber(n))
                {
                    sum += n;
                    count--;
                }

                n--;
            }

            return sum;
        }
    }
}
