﻿namespace WhileStatements
{
    public static class GeometricSequences
    {
        public static uint SumGeometricSequenceTerms1(uint a, uint r, uint n)
        {
            uint sum = 0, i = 0;

            while (i < n)
            {
                sum += (uint)Math.Pow(r, i++) * a;
            }

            return sum;
        }

        public static uint SumGeometricSequenceTerms2(uint n)
        {
            return SumGeometricSequenceTerms1(13, 3, n);
        }

        public static uint CountGeometricSequenceTerms3(uint a, uint r, uint maxTerm)
        {
            uint term = a;
            uint count = 0;

            while (term <= maxTerm)
            {
                count++;
                term *= r;
            }

            return count;
        }

        public static uint CountGeometricSequenceTerms4(uint a, uint r, uint n, uint minTerm)
        {
            uint i = 0;
            uint count = 0;
            uint term = a;

            while (i++ < n)
            {
                count += term >= minTerm ? 1u : 0u;
                term *= r;
            }

            return count;
        }
    }
}
