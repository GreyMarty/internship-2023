﻿namespace WhileStatements
{
    public static class ArithmeticSequences
    {
        public static int SumArithmeticSequenceTerms1(int a, int n)
        {
            int i = 0;
            int sum = 0;

            while (i < n)
            {
                sum += a + i++;
            }

            return sum;
        }

        public static int SumArithmeticSequenceTerms2(int n)
        {
            int a0 = 17;
            int d = 33;

            int sum = 0, i = 0;

            while (i < n)
            {
                sum += a0 + (d * i++);
            }

            return sum;
        }

        public static int SumArithmeticSequenceTerms3(int a, int n)
        {
            int d = 3;
            int sum = 0, i = 0;

            while (i < n)
            {
                sum += a + (d * i++);
            }

            return sum;
        }

        public static int SumArithmeticSequenceTerms4(int a, int d, int n)
        {
            int sum = 0, i = 0;

            while (i < n)
            {
                sum += a + (d * i++);
            }

            return sum;
        }
    }
}
