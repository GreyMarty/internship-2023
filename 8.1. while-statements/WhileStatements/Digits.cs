﻿namespace WhileStatements
{
    public static class Digits
    {
        public static ulong GetDigitsSum(ulong n)
        {
            ulong sum = 0;

            while (n > 0)
            {
                sum += n % 10u;
                n /= 10u;
            }

            return sum;
        }

        public static ulong GetDigitsProduct(ulong n)
        {
            ulong product = n > 0u ? 1u : 0u;

            while (n > 0)
            {
                product *= n % 10u;
                n /= 10u;
            }

            return product;
        }
    }
}
