using System.Text;

namespace Streams
{
    public static class ReadingFromStream
    {
        public static string ReadAllStreamContent(StreamReader streamReader)
        {
            return streamReader.ReadToEnd();
        }

        public static string[] ReadLineByLine(StreamReader streamReader)
        {
            var result = new List<string>();

            while (!streamReader.EndOfStream)
            {
                result.Add(streamReader.ReadLine() ?? string.Empty);
            }

            return result.ToArray();
        }

        public static StringBuilder ReadOnlyLettersAndNumbers(StreamReader streamReader)
        {
            var builder = new StringBuilder();

            while (!streamReader.EndOfStream)
            {
                var chr = (char)streamReader.Peek();

                if (!char.IsLetterOrDigit(chr))
                {
                    break;
                }

                _ = streamReader.Read();
                _ = builder.Append(chr);
            }

            return builder;
        }

        public static char[][] ReadAsCharArrays(StreamReader streamReader, int arraySize)
        {
            var result = new List<char[]>();
            var buffer = new char[arraySize];

            while (!streamReader.EndOfStream)
            {
                var count = streamReader.Read(buffer, 0, buffer.Length);
                result.Add(buffer[0..count]);
            }

            return result.ToArray();
        }
    }
}
