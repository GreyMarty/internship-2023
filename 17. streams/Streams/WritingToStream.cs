using System.Globalization;
using System.Text;

namespace Streams
{
    public static class WritingToStream
    {
        public static void ReadAndWriteIntegers(StreamReader streamReader, StreamWriter outputWriter)
        {
            while (!streamReader.EndOfStream)
            {
                outputWriter.Write(streamReader.Read().ToString(CultureInfo.InvariantCulture));
            }
        }

        public static void ReadAndWriteChars(StreamReader streamReader, StreamWriter outputWriter)
        {
            outputWriter.Write(streamReader.ReadToEnd());
            outputWriter.Flush();
        }

        public static void TransformBytesToHex(StreamReader contentReader, StreamWriter outputWriter)
        {
            while (!contentReader.EndOfStream)
            {
                var value = contentReader.Read();
                outputWriter.Write(value.ToString("X2", CultureInfo.InvariantCulture));
            }

            outputWriter.Flush();
        }

        public static void WriteLinesWithNumbers(StreamReader contentReader, StreamWriter outputWriter)
        {
            var i = 1;

            while (!contentReader.EndOfStream)
            {
                var line = contentReader.ReadLine();
                outputWriter.WriteLine($"{i:000} {line}");
                i++;
            }

            outputWriter.Flush();
        }

        public static void RemoveWordsFromContentAndWrite(StreamReader contentReader, StreamReader wordsReader, StreamWriter outputWriter)
        {
            var words = new HashSet<string>(wordsReader.ReadToEnd().Split("\n"));

            var wordBuilder = new StringBuilder();
            string word;

            while (!contentReader.EndOfStream)
            {
                var chr = (char)contentReader.Read();

                if (!char.IsLetterOrDigit(chr))
                {
                    word = wordBuilder.ToString();

                    if (!words.Contains(word))
                    {
                        outputWriter.Write(word);
                    }

                    _ = wordBuilder.Clear();
                    outputWriter.Write(chr);
                }
                else
                {
                    _ = wordBuilder.Append(chr);
                }
            }

            word = wordBuilder.ToString();

            if (!words.Contains(word))
            {
                outputWriter.Write(word);
            }

            outputWriter.Flush();
        }
    }
}
