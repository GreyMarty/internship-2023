namespace Streams
{
    public static class ReadingFromString
    {
        public static string ReadAllStreamContent(StringReader stringReader)
        {
            return stringReader.ReadToEnd();
        }

        public static string ReadCurrentLine(StringReader stringReader)
        {
            return stringReader.ReadLine() ?? string.Empty;
        }

        public static bool ReadNextCharacter(StringReader stringReader, out char currentChar)
        {
            var result = stringReader.Read();

            if (result == -1)
            {
                currentChar = ' ';
                return false;
            }

            currentChar = (char)result;
            return true;
        }

        public static bool PeekNextCharacter(StringReader stringReader, out char currentChar)
        {
            var result = stringReader.Peek();

            if (result == -1)
            {
                currentChar = ' ';
                return false;
            }

            currentChar = (char)result;
            return true;
        }

        public static char[] ReadCharactersToBuffer(StringReader stringReader, int count)
        {
            var buffer = new char[count];

            var actualCount = stringReader.Read(buffer, 0, buffer.Length);
            return buffer[0..actualCount];
        }
    }
}
