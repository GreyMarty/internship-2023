﻿using System;

// ReSharper disable InconsistentNaming
namespace QuickSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with quick sort algorithm.
        /// </summary>
        public static void QuickSort(this int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length < 2)
            {
                return;
            }

            var stack = new Stack<int>();
            stack.Push(0);
            stack.Push(array.Length - 1);

            while (stack.Count > 0)
            {
                var end = stack.Pop();
                var start = stack.Pop();

                var pivot = Partition(array, start, end);

                if (start < pivot)
                {
                    stack.Push(start);
                    stack.Push(pivot);
                }

                if (end > pivot + 1)
                {
                    stack.Push(pivot + 1);
                    stack.Push(end);
                }
            }
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive quick sort algorithm.
        /// </summary>
        public static void RecursiveQuickSort(this int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            QuickSort(array, 0, array.Length - 1);
        }

        private static void QuickSort(int[] array, int start, int end)
        {
            if (start >= end)
            {
                return;
            }

            var pivot = Partition(array, start, end);
            QuickSort(array, start, pivot);
            QuickSort(array, pivot + 1, end);
        }

        private static int Partition(int[] array, int start, int end)
        {
            var pivot = array[((end - start) / 2) + start];

            int i = start - 1;
            int j = end + 1;

            while (true)
            {
                i++;

                while (array[i] < pivot)
                {
                    i++;
                }

                j--;

                while (array[j] > pivot)
                {
                    j--;
                }

                if (i >= j)
                {
                    return j;
                }

                Swap(ref array[i], ref array[j]);
            }
        }

        private static void Swap<T>(ref T a, ref T b)
        {
            var temp = a;
            a = b;
            b = temp;
        }
    }
}
