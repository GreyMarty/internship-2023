﻿using System;
using System.Text;

namespace Sequences
{
    public static class Sequences
    {
        /// <summary>
        /// Find all the contiguous substrings of "length" length in given string of digits in the order that they appear.
        /// </summary>
        /// <param name="numbers">Source string.</param>
        /// <param name="length">Length of substring.</param>
        /// <returns>All the contiguous substrings of length n in that string in the order that they appear.</returns>
        /// <exception cref="ArgumentException">
        /// Throw if length of substring less and equals zero
        /// -or-
        /// more than length of source string
        /// - or-
        /// source string containing a non-digit character
        /// - or
        /// source string is null or empty or white space.
        /// </exception>
        public static string[] GetSubstrings(string numbers, int length)
        {
            if (length <= 0)
            {
                throw new ArgumentException($"{nameof(length)} must be greater than zero.", nameof(length));
            }

            if (length > numbers.Length)
            {
                throw new ArgumentException($"{nameof(length)} must be less or equal to {nameof(numbers)}.Length.", nameof(length));
            }

            if (string.IsNullOrWhiteSpace(numbers))
            {
                throw new ArgumentException($"{nameof(numbers)} must not be empty.", nameof(numbers));
            }

            if (numbers.Any(c => c < '0' || c > '9'))
            {
                throw new ArgumentException($"{nameof(numbers)} must contain olny digit characters.", nameof(numbers));
            }

            var n = numbers.Length - length + 1;
            var result = new string[n];

            for (int i = 0; i < n; i++)
            {
                result[i] = numbers.Substring(i, length);
            }

            return result;
        }
    }
}
