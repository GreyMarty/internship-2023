﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.SymbolStore;

namespace FilterByPalindromicTask
{
    /// <summary>
    /// Provides static method for working with integers array.
    /// </summary>
    public static class ArrayExtension
    {
        /// <summary>
        /// Returns new array that contains only palindromic numbers from source array.
        /// </summary>
        /// <param name="source">Source array.</param>
        /// <returns>Array of elements that are palindromic numbers.</returns>
        /// <exception cref="ArgumentNullException">Throw when array is null.</exception>
        /// <exception cref="ArgumentException">Throw when array is empty.</exception>
        /// <example>
        /// {12345, 1111111112, 987654, 56, 1111111, -1111, 1, 1233321, 70, 15, 123454321}  => { 1111111, 123321, 123454321 }
        /// {56, -1111111112, 987654, 56, 890, -1111, 543, 1233}  => {  }.
        /// </example>
        public static int[] FilterByPalindromic(int[]? source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException($"{nameof(source)} must not be null.", nameof(source));
            }

            var palindroms = new List<int>();

            foreach (int i in source)
            {
                if (IsPalindrom(i))
                {
                    palindroms.Add(i);
                }
            }

            return palindroms.ToArray();

            throw new NotImplementedException("You need to implement this function.");
        }

        private static bool IsPalindrom(int value)
        {
            if (value < 0 || value == int.MaxValue)
            {
                return false;
            }

            value = Math.Abs(value);
            var length = GetLength(value);

            for (int i = 0; i < length / 2; i++)
            {
                if (GetDigit(value, i) != GetDigit(value, length - i - 1))
                {
                    return false;
                }
            }

            return true;
        }

        private static int GetDigit(int value, int index) => (value / (int)Math.Pow(10, index)) % 10;

        private static int GetLength(int value) => value == 0 ? 1 : (int)Math.Log10(value) + 1;
    }
}
