﻿using System;
using System.Security.AccessControl;
using System.Text;

namespace LanguageGame
{
    public static class Translator
    {
        /// <summary>
        /// Translates from English to Pig Latin. Pig Latin obeys a few simple following rules:
        /// - if word starts with vowel sounds, the vowel is left alone, and most commonly 'yay' is added to the end;
        /// - if word starts with consonant sounds or consonant clusters, all letters before the initial vowel are
        ///   placed at the end of the word sequence. Then, "ay" is added.
        /// Note: If a word begins with a capital letter, then its translation also begins with a capital letter,
        /// if it starts with a lowercase letter, then its translation will also begin with a lowercase letter.
        /// </summary>
        /// <param name="phrase">Source phrase.</param>
        /// <returns>Phrase in Pig Latin.</returns>
        /// <exception cref="ArgumentException">Thrown if phrase is null or empty.</exception>
        /// <example>
        /// "apple" -> "appleyay"
        /// "Eat" -> "Eatyay"
        /// "explain" -> "explainyay"
        /// "Smile" -> "Ilesmay"
        /// "Glove" -> "Oveglay".
        /// </example>
        public static string TranslateToPigLatin(string phrase)
        {
            if (string.IsNullOrWhiteSpace(phrase))
            {
                throw new ArgumentException($"{phrase} must not be empty.");
            }

            var resultBuilder = new StringBuilder();

            var wordStart = 0;
            var wordLength = 0;

            for (int i = 0; i < phrase.Length; i++)
            {
                var chr = phrase[i];
                var lowerChr = char.ToLowerInvariant(chr);

                if ((lowerChr >= 'a' && lowerChr <= 'z') || lowerChr == '’')
                {
                    if (wordLength == 0)
                    {
                        wordStart = i;
                    }

                    wordLength++;
                }
                else
                {
                    if (wordLength > 0)
                    {
                        resultBuilder.Append(TranslateWordToPigLatin(phrase, wordStart, wordLength));
                        wordLength = 0;
                    }

                    resultBuilder.Append(chr);
                }
            }

            if (wordLength > 0)
            {
                resultBuilder.Append(TranslateWordToPigLatin(phrase, wordStart, wordLength));
            }

            return resultBuilder.ToString();
        }

        private static string TranslateWordToPigLatin(string str, int startIndex, int count)
        {
            var endIndex = startIndex + count;

            bool capitalize = char.IsUpper(str[startIndex]);

            var vowels = new[] { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };
            var firstVowelIndex = str.IndexOfAny(vowels, startIndex, count);

            string translation;

            if (firstVowelIndex == startIndex)
            {
                translation = $"{str[startIndex..endIndex]}yay";
            }
            else if (firstVowelIndex >= 0)
            {
                translation = $"{str[firstVowelIndex..endIndex]}{str[startIndex..firstVowelIndex]}ay";
            }
            else
            {
                translation = $"{str[startIndex..endIndex]}ay";
            }

            translation = translation.ToLowerInvariant();
            return capitalize ? translation.Capitalize() : translation;
        }

        private static string Capitalize(this string str) => $"{char.ToUpperInvariant(str[0])}{str[1..]}";
    }
}
