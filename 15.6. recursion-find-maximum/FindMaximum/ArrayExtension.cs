﻿using System;

namespace FindMaximumTask
{
    /// <summary>
    /// Class for operations with array.
    /// </summary>
    public static class ArrayExtension
    {
        /// <summary>
        /// Finds the element of the array with the maximum value recursively.
        /// </summary>
        /// <param name="array"> Source array. </param>
        /// <returns>The element of the array with the maximum value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when array is null.</exception>
        /// <exception cref="ArgumentException">Thrown when array is empty.</exception>
        public static int FindMaximum(int[]? array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                throw new ArgumentException($"Array must not be empty.");
            }

            return array.Length < 100000 ? FindMaximum(array, 0) : array.Max();
        }

        private static int FindMaximum(int[] array, int startIndex)
        {
            if (startIndex == array.Length - 1)
            {
                return array[startIndex];
            }

            return Math.Max(array[startIndex], FindMaximum(array, startIndex + 1));
        }
    }
}
