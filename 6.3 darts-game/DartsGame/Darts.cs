﻿using System;

namespace DartsGame
{
    public static class Darts
    {
        /// <summary>
        /// Calculates the earned points in a single toss of a Darts game.
        /// </summary>
        /// <param name="x">x-coordinate of dart.</param>
        /// <param name="y">y-coordinate of dart.</param>
        /// <returns>The earned points.</returns>
        public static int GetScore(double x, double y)
        {
            var distance = (x * x) + (y * y);

            return distance switch
            {
                <= 1 => 10,
                <= 25 => 5,
                <= 100 => 1,
                _ => 0
            };
        }
    }
}
