﻿using System;

namespace PalindromicNumberTask
{
    /// <summary>
    /// Provides static method for working with integers.
    /// </summary>
    public static class NumbersExtension
    {
        /// <summary>
        /// Determines if a number is a palindromic number, see https://en.wikipedia.org/wiki/Palindromic_number.
        /// </summary>
        /// <param name="number">Verified number.</param>
        /// <returns>true if the verified number is palindromic number; otherwise, false.</returns>
        /// <exception cref="ArgumentException"> Thrown when source number is less than zero. </exception>
        public static bool IsPalindromicNumber(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException($"{nameof(number)} must be greater or equal to zero.");
            }

            if (number < 10)
            {
                return true;
            }

            var length = (int)Math.Log10(number) + 1;

            var exp = (int)Math.Pow(10, length - 1);

            var firstDigit = number / exp;
            var lastDigit = number % 10;

            if (firstDigit != lastDigit)
            {
                return false;
            }

            return IsPalindromicNumber((number % exp) / 10);
        }
    }
}
